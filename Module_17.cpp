#include <iostream>
#include <cmath>

using namespace std;

class Vector 
{
private:
	double x;
	double y;
	double z;

public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void printVector()
	{
		cout << x << " " << y << " " << z;
	}

	void setVector(double newX, double newY, double newZ)
	{
		x = newX;
		y = newY;
		z = newZ;
	}

	int vectorLength()
	{
		double length = 0;
		length = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
		return length;
	}
};


int main()
{
	Vector vec(10,5,2);

	vec.printVector();
	//vec.setVector(8, 6, 3);
	cout << "\n";
	cout << vec.vectorLength();
}
